package com.demo.stock.stockstastics.response;

public class ResponseCode {

	private ResponseCode() {

	}

	public static final int UNAUTHORIZED = 401;
	public static final int SUCCESS = 200;
	public static final int BADREQUEST = 400;
	public static final int INTERNALSERVER = 500;
	public static final int CREATED = 201;
	public static final int NOT_FOUND = 404;
	public static final int INVALID_DATA = 1001;
	public static final int UNPROCESSABLE_ENTITY = 422;
	
}
