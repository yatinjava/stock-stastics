package com.demo.stock.stockstastics.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
public class DataResponse extends Response {

	Map<String, Object> data;
	
	public DataResponse(int code, String message, Map<String, Object> data) {
		super(code, message);
		this.data = data;
	}

	public DataResponse(HttpStatus httpstatuscode,int code, String message, Map<String, Object> data) {
		super(httpstatuscode,code, message);
		this.data = data;
	}



	public Map<String, Object> getData() {
		return data;
	}

	public void setData(Map<String, Object> data) {
		this.data = data;
	}

}
