package com.demo.stock.stockstastics.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@NoArgsConstructor
@Getter
@Setter
@AllArgsConstructor
public class DataTableResponse {

	private Map<String, Object> data;
	private Long totalRecoreds;
	private String responseMessage;
	private int responseCode;

}
