package com.demo.stock.stockstastics.model;

import java.util.List;


public class Investors {

	private List<InvestorDetails> investorsList;

	public List<InvestorDetails> getInvestorsList() {
		return investorsList;
	}

	public void setInvestorsList(List<InvestorDetails> investorsList) {
		this.investorsList = investorsList;
	}
	
	
}
