package com.demo.stock.stockstastics.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvestorDetails {

	private Integer userId;
	private Double totalInvestments;
	private Double totalStocks;
	
	public Integer getUserId() {
		return userId;
	}


	public void setUserId(Integer userId) {
		this.userId = userId;
	}


	public Double getTotalInvestments() {
		return totalInvestments;
	}
	public void setTotalInvestments(Double totalInvestments) {
		this.totalInvestments = totalInvestments;
	}
	public Double getTotalStocks() {
		return totalStocks;
	}
	public void setTotalStocks(Double totalStocks) {
		this.totalStocks = totalStocks;
	}
	
	
	
	
}
