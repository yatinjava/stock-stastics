package com.demo.stock.stockstastics.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockAnalysis {

    private Integer year;
    private Double maxPrice;
    private Double lowPrice;
    private Long stockId;

}
