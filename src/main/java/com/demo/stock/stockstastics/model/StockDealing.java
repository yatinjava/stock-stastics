package com.demo.stock.stockstastics.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockDealing {
    private String stockName;

    private Double purchaseQnty;

    @Override
    public String toString() {
        return "StockDealing{" +
                "stockName='" + stockName + '\'' +
                ", purchaseQnty=" + purchaseQnty +
                ", purchaseDate=" + purchaseDate +
                ", purchasedPrice=" + purchasedPrice +
                ", currentPrice=" + currentPrice +
                ", totalDealingPrice=" + totalDealingPrice +
                ", dealingDate=" + dealingDate +
                ", dealingType='" + dealingType + '\'' +
                '}';
    }

    private Instant purchaseDate;

    private Double purchasedPrice;

    private Double currentPrice;

    private Double totalDealingPrice;

    private Instant dealingDate;

    private String dealingType;

}
