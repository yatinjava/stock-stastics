package com.demo.stock.stockstastics.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.*;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author Emxcel
 * This for load classpath configuration file.
 */
@Configuration
@EnableAutoConfiguration
@EntityScan({ "com.demo.stock.stockstastics.model", "com.demo.stock.stockstastics.entity" }) // entityscan and component scan we can exclude also
																	// because main annoation will
// do this by default
@ComponentScan({ "com.demo.stock.stockstastics.config", "com.demo.stock.stockstastics.service", "com.demo.stock.stockstastics.impl",
		"com.demo.stock.stockstastics.repository", "com.demo.stock.stockstastics.controller", "com.demo.stock.stockstastics.model", "com.demo.stock.stockstastics.entity",
		"com.demo.stock.stockstastics", "com.demo.stock.stockstastics.utils" ,"com.demo.stock.stockstastics.batchjob" })
@PropertySources({ @PropertySource("classpath:messages.properties") })
@EnableTransactionManagement
@EnableAsync
public class CoreApplicationConfiguration {
	}