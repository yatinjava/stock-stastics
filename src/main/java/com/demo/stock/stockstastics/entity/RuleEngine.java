package com.demo.stock.stockstastics.entity;

import lombok.*;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@Table("ruleengine")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RuleEngine {

    private static final long serialVersionUID = 186L;

    @Column("userId")
    private Integer userId;

    @Column("stockId")
    private Long stockId;

    @Column("configPrice")
    private BigDecimal configPrice;

    @Column("decision")
    private Integer decision;

    @Column("runStatus")
    private Integer runStatus;

    @Column("quantity")
    private Integer quantity;

    @NonNull
    @Column("update_date")
    private Instant update_date;

}
