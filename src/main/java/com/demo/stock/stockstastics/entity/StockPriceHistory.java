package com.demo.stock.stockstastics.entity;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.math.BigDecimal;
import java.time.Instant;


@Data
@Table("stockprice_history")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StockPriceHistory {

    private static final long serialVersionUID = 1679L;

    @Id
    @Column("id")
    private Integer id;

    @Column("stockId")
    private Long stockId;

    @Column("stock_price")
    private BigDecimal stockPrice;

    @NonNull
    @Column("update_date")
    private Instant update_date;

}
