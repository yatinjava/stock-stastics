package com.demo.stock.stockstastics.entity;


import lombok.*;

import java.math.BigDecimal;
import java.time.Instant;


import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;


@Data
@Table("orderhistory")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Orderhistory {

    private static final long serialVersionUID = 167L;

    @Id
    @Column("id")
    private Integer id;

    @Column("userId")
    private Integer userId;

    @Column("stockCode")
    private Long stockCode;

    @Column("purchasedPrice")
    private BigDecimal purchasedPrice;

    @Column("quantity")
    private Integer quantity;

    @NonNull
    @Column("purchasedDate")
    private Instant purchasedDate;

}
