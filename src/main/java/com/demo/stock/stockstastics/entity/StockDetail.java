package com.demo.stock.stockstastics.entity;

import java.math.BigDecimal;

import java.time.Instant;

import lombok.*;
import java.time.LocalDateTime;


import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@Table("stock_details")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StockDetail {

    private static final long serialVersionUID = 166L;

    @Id
    @Column("stocks_id")
    private Long stocks_id;

    @Column("stock_name")
    private String stockName;

    @Column("stock_description")
    private String stockDescription;

    @Column("stock_price")
    private BigDecimal stockPrice;

    
    @Column("stock_date")
    private LocalDateTime stockDate;

}
