package com.demo.stock.stockstastics.entity;

import java.time.Instant;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class StocksResponse {
	
	private String stockName;
	
	private Double quantity;
	
	private Instant purchasedDate;
	
	private Double purchasedPrice;
	
	

}
