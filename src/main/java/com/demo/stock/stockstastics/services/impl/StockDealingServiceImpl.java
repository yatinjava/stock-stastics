package com.demo.stock.stockstastics.services.impl;

import com.demo.stock.stockstastics.constants.MessageConstants;
import com.demo.stock.stockstastics.entity.RuleEngine;
import com.demo.stock.stockstastics.exception.GlobalException;
import com.demo.stock.stockstastics.exception.ResourceNotFoundException;
import com.demo.stock.stockstastics.model.StockAnalysis;
import com.demo.stock.stockstastics.model.StockDealing;
import com.demo.stock.stockstastics.repository.CustomStockDetailRepository;
import com.demo.stock.stockstastics.repository.RuleEngineRepository;
import com.demo.stock.stockstastics.services.StockDealingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;


@Service
public class StockDealingServiceImpl implements StockDealingService {

    @Autowired
    private RuleEngineRepository ruleEngineRepository;

    @Autowired
    private CustomStockDetailRepository customStockDetailRepository;


    @Override
    public Flux<StockDealing> getStockDealing(Integer userId) {

        return ruleEngineRepository.findRuleEngineByUserIdAndRunStatus(userId, 0)
                .delayElements(Duration.ofSeconds(1))
                .doOnNext(i -> System.out.println("Processing rule engine" + i))
                .flatMap(ruleEngine -> {
                            return processingRule(ruleEngine, userId);
                        }
                ).switchIfEmpty(Mono.defer(() -> Mono.error(new GlobalException(HttpStatus.NOT_FOUND, String.format("Stock not found for the provided userId :: %s", userId)))));
    }

    @Override
    public Flux<StockAnalysis> getStockAnalysis(Long stockId) {
        return customStockDetailRepository.getStockAnalysisDetails(stockId)
                .switchIfEmpty(Mono.defer(() -> Mono.error(new GlobalException(HttpStatus.NOT_FOUND, String.format("Stock not found for the provided stockId :: %s", stockId)))));
    }

    private Mono<StockDealing> processingRule(RuleEngine ruleEngine, Integer userId) {
        return customStockDetailRepository.getStockInformationByUserWise(ruleEngine.getStockId(), userId).flatMap(value -> {
            value.setTotalDealingPrice(setDealingPrice(value, ruleEngine.getDecision(), ruleEngine.getQuantity(), ruleEngine.getConfigPrice().doubleValue()));
            value.setDealingType((ruleEngine.getDecision() == MessageConstants.SELL_FLAG) ? "SELL IT" : "BUY IT");
            return Mono.just(value);
        });
    }

    private Double setDealingPrice(StockDealing stockDealing, int decisionFlag, int dealQuantity, double configPrice) {
        // If current stock price is greter than the configured price of rule engine , then pls sell it
        if (decisionFlag == MessageConstants.SELL_FLAG && configPrice < stockDealing.getCurrentPrice()) {
            return (dealQuantity * stockDealing.getCurrentPrice()) - (stockDealing.getPurchaseQnty() * stockDealing.getPurchasedPrice());
        }
        // If current stock price is lesser than the configured price of rule engine , then pls buy it
        else if (decisionFlag == MessageConstants.BUY_FLAG && configPrice > stockDealing.getCurrentPrice()) {
            return (dealQuantity * stockDealing.getCurrentPrice()) + (stockDealing.getPurchaseQnty() * stockDealing.getPurchasedPrice());
        }
        return 0.0;

    }
}
