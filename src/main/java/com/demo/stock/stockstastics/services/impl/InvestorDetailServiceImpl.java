package com.demo.stock.stockstastics.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.stock.stockstastics.model.Investors;
import com.demo.stock.stockstastics.repository.CustomStockDetailRepository;
import com.demo.stock.stockstastics.services.InvestorDetailService;

import reactor.core.publisher.Mono;

@Service
public class InvestorDetailServiceImpl implements InvestorDetailService {

	@Autowired
	private CustomStockDetailRepository customStockDetailRepository;

	@Override
	public Mono<Investors> getInvestorDetails() {

		Investors investor = new Investors();

		Mono<Investors> map = customStockDetailRepository.getInvestorDetails().collectList().map(mapper -> {
			investor.setInvestorsList(mapper);
			return investor;
		});

		return map;
	}

}
