package com.demo.stock.stockstastics.services.impl;

import com.demo.stock.stockstastics.entity.StockDetail;
import com.demo.stock.stockstastics.exception.ResourceNotFoundException;
import com.demo.stock.stockstastics.model.StockDealing;
import com.demo.stock.stockstastics.repository.CustomStockDetailRepository;
import com.demo.stock.stockstastics.repository.StocksRepository;
import com.demo.stock.stockstastics.services.StockDetailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;


@Service
public class StockDetailServiceImpl implements StockDetailService {
    @Autowired
    private StocksRepository stockDetailRepository;

    @Autowired
    private CustomStockDetailRepository customStockDetailRepository;

    private static final Logger logger = LogManager.getLogger(StockDetailServiceImpl.class);


    @Override
    public Mono<StockDetail> getStockDetail(String stockName) throws ResourceNotFoundException {
        return getStockDetailByStockName(stockName);
    }

    private Mono<StockDetail> getStockDetailByStockName(String stockName) throws ResourceNotFoundException {
        return stockDetailRepository.findByStockName(stockName)
                .switchIfEmpty(Mono.defer(() -> Mono.error(new ResourceNotFoundException(
                        String.format("Stock not found for the provided stockName :: %s", stockName)))));
    }

    public Mono<StockDealing> getStockDetailByStockId(Long stockId, Integer userId) {
        return customStockDetailRepository.getStockInformationByUserWise(stockId, userId);
    }


}
