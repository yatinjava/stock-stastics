package com.demo.stock.stockstastics.services;

import com.demo.stock.stockstastics.exception.ResourceNotFoundException;
import com.demo.stock.stockstastics.model.StockAnalysis;
import com.demo.stock.stockstastics.model.StockDealing;
import reactor.core.publisher.Flux;

public interface StockDealingService {

    Flux<StockDealing> getStockDealing(Integer userId);

    Flux<StockAnalysis> getStockAnalysis(Long stockId);


}
