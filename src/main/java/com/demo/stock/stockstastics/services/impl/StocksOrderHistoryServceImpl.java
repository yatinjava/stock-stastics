package com.demo.stock.stockstastics.services.impl;

import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.demo.stock.stockstastics.entity.StocksResponse;
import com.demo.stock.stockstastics.repository.OrderHistoryRepository;
import com.demo.stock.stockstastics.services.StocksOrderHstoryServce;
import reactor.core.publisher.Flux;

/**
 * This is service class used to fetch user ordered stocks
 * @author sidramesh.mudhol
 *
 */
@Service
public class StocksOrderHistoryServceImpl implements StocksOrderHstoryServce {

	
	private static final Logger logger = LogManager.getLogger(StocksOrderHistoryServceImpl.class);

	@Autowired
	OrderHistoryRepository orderHistoryRepository;
	
	@Override
	public Flux<StocksResponse> getStocksByUser(int userId) {
		logger.info("inside getStocksByuser() method");
		return orderHistoryRepository.getOrderHistoryBasedonUser(userId).delayElements(Duration.ofMillis(1000));
	}

}
