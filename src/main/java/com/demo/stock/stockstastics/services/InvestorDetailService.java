package com.demo.stock.stockstastics.services;

import com.demo.stock.stockstastics.model.Investors;

import reactor.core.publisher.Mono;

public interface InvestorDetailService {

	Mono<Investors> getInvestorDetails();
}
