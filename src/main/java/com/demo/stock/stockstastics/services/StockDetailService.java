package com.demo.stock.stockstastics.services;


import com.demo.stock.stockstastics.entity.StockDetail;
import com.demo.stock.stockstastics.exception.ResourceNotFoundException;
import reactor.core.publisher.Mono;

public interface StockDetailService {


    Mono<StockDetail> getStockDetail(String stockName) throws ResourceNotFoundException;

}