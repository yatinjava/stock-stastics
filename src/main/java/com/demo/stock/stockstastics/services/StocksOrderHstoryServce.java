package com.demo.stock.stockstastics.services;

import org.springframework.stereotype.Service;

import com.demo.stock.stockstastics.entity.StocksResponse;

import reactor.core.publisher.Flux;

@Service
public interface StocksOrderHstoryServce {
	
	
	Flux<StocksResponse> getStocksByUser(int userId);
		
	

}
