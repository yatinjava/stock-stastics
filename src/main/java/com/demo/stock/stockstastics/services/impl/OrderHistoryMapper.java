package com.demo.stock.stockstastics.services.impl;

import java.time.Instant;
import java.util.function.BiFunction;

import com.demo.stock.stockstastics.entity.StocksResponse;

import io.r2dbc.spi.Row;

public class OrderHistoryMapper implements BiFunction<Row, Object, StocksResponse> {

	@Override
	public StocksResponse apply(Row row, Object u) {
		StocksResponse stocksResponse = new StocksResponse();
		  stocksResponse.setStockName( row.get("stockName", String.class));
		  stocksResponse.setQuantity(row.get("quantity", Double.class));
		  stocksResponse.setPurchasedDate(row.get("purchasedDate", Instant.class));
		  stocksResponse.setPurchasedPrice(row.get("purchasedPrice", Double.class));
		  return stocksResponse;
	}
	
	
	 
	    
		
		

}
