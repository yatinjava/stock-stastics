package com.demo.stock.stockstastics.constants;

public class UrlConstants {

    public static final String BASE_URL = "/";
    public static final String HEALTH_CHECK = "/healthCheck";
    public static final String GET_STOCK = "/stock/{stockName}";
    public static final String GET_STOCK_ANALYSIS = "/stock/analysis/{stockId}";
    public static final String STOCK_DEALING = "/stockDealing/{userId}";
    public static final String STOCK_DEALINGSTREM = "/strstockDealing/{userId}";
    public static final String TRADERS_OVERVIEW = "/tradersOverview";
    public static final String ORDER_HISTORY = "/users/{userid}/stocks";


    private UrlConstants(){

    }

}
