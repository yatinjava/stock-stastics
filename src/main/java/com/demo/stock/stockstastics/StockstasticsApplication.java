package com.demo.stock.stockstastics;

import com.demo.stock.stockstastics.model.StockAnalysis;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@SpringBootApplication
public class StockstasticsApplication {

    public static void main(String[] args) {
        SpringApplication.run(StockstasticsApplication.class, args);

       /* StockAnalysis st1 = new StockAnalysis(2020, 20d, 11l);
        StockAnalysis st2 = new StockAnalysis(2020, 30d, 11l);

        StockAnalysis st3 = new StockAnalysis(2020, 60d, 11l);
        StockAnalysis st4 = new StockAnalysis(2021, 33d, 12l);
        StockAnalysis st5 = new StockAnalysis(2021, 67d, 12l);
        StockAnalysis st6 = new StockAnalysis(2021, 47d, 12l);

        List<StockAnalysis> dblist = new ArrayList<StockAnalysis>();
        dblist.add(st1);
        dblist.add(st2);
        dblist.add(st3);
        dblist.add(st4);
        dblist.add(st5);
        dblist.add(st6);

        Map<Integer, List<StockAnalysis>> result = dblist.stream()
                .collect(groupingBy(StockAnalysis::getYear));
*/


    }

}
