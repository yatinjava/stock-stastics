package com.demo.stock.stockstastics.controller;


import com.demo.stock.stockstastics.constants.MessageConstants;
import com.demo.stock.stockstastics.constants.UrlConstants;
import com.demo.stock.stockstastics.entity.StockDetail;
import com.demo.stock.stockstastics.exception.ResourceNotFoundException;
import com.demo.stock.stockstastics.model.Investors;
import com.demo.stock.stockstastics.model.StockAnalysis;
import com.demo.stock.stockstastics.model.StockDealing;
import com.demo.stock.stockstastics.response.Response;
import com.demo.stock.stockstastics.services.InvestorDetailService;
import com.demo.stock.stockstastics.services.StockDealingService;
import com.demo.stock.stockstastics.services.StockDetailService;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@Validated
public class StockController {

    private static final Logger logger = LogManager.getLogger(StockController.class);

    @Autowired
    private StockDetailService stockDetailService;

    @Autowired
    private StockDealingService stockDealingService;

    @Autowired
    private InvestorDetailService investorDetailService;


    @GetMapping(value = "/ping")
    public String home() {
        return MessageConstants.WELCOME_MESSAGE;
    }

    @GetMapping(value = UrlConstants.HEALTH_CHECK, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> healthCheck() {
        logger.info("Got request to check health status");
        return ResponseEntity.ok(new Response(HttpStatus.OK, HttpStatus.OK.value(), MessageConstants.SUCCESS));
    }

    @GetMapping(value = UrlConstants.GET_STOCK, produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<StockDetail>
    getCatalogueItemBySKU(@PathVariable(value = "stockName") String stockName)
            throws ResourceNotFoundException {

        return stockDetailService.getStockDetail(stockName);
    }


    @GetMapping(value = UrlConstants.STOCK_DEALING, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public Flux<StockDealing>
    getStockDealingList(@PathVariable(value = "userId") Integer userId)  {
        Flux<StockDealing> data = stockDealingService.getStockDealing(userId);
        data.subscribe(System.out::print);
        return data;
    }

    @GetMapping(value = UrlConstants.TRADERS_OVERVIEW, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Mono<Investors> getTradersOverView() {
        logger.info("Get Traders OverView ");
        return investorDetailService.getInvestorDetails();
    }

    @GetMapping(value = UrlConstants.GET_STOCK_ANALYSIS, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<StockAnalysis> getStockAnalysis(@PathVariable(value = "stockId") Long stockId) {
        logger.info("Get Stock analysis ");
        return stockDealingService.getStockAnalysis(stockId);
    }




}
