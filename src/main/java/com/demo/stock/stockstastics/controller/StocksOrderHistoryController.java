package com.demo.stock.stockstastics.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.demo.stock.stockstastics.constants.UrlConstants;
import com.demo.stock.stockstastics.entity.StocksResponse;
import com.demo.stock.stockstastics.services.StocksOrderHstoryServce;

import reactor.core.publisher.Flux;
/**
 * This is controller class to fetch user stock history
 * @author sidramesh.mudhol
 *
 */
@RestController
public class StocksOrderHistoryController {
	
	
	private static final Logger logger = LogManager.getLogger(StocksOrderHistoryController.class);
	
	@Autowired
	private StocksOrderHstoryServce orderHstoryServce;
	
	/**
	 * 
	 * @param userId, may not be null
	 * @return StocksResponse
	 */
	@GetMapping(value = UrlConstants.ORDER_HISTORY, produces = MediaType.TEXT_EVENT_STREAM_VALUE)
	private Flux<StocksResponse> getAllStocksByUser(@PathVariable("userid") int userId) {
		
		logger.info("inside getAllStocksByUser() method ");
		return orderHstoryServce.getStocksByUser(userId);
	}

}
