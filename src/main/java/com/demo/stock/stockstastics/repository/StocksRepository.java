package com.demo.stock.stockstastics.repository;

import com.demo.stock.stockstastics.entity.StockDetail;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import reactor.core.publisher.Mono;

public interface StocksRepository extends ReactiveSortingRepository<StockDetail, Long> {

    Mono<StockDetail> findByStockName(String stockName);

   /* @Query(value = "SELECT new com.demo.stock.stockstastics.model.StockDealing(sd.stockName as stockName, odh.quantity as purchaseQnty, " +
            "   odh.purchasedDate as purchaseDate, odh.purchasedPrice as purchasedPrice, sd.stockPrice as currentPrice, " +
            "   0 as totalDealingPrice,now() as dealingDate,'' as dealingType)" +
            "   FROM Orderhistory odh " +
            "   INNER JOIN  StockDetail sd  ON sd.stocks_id = odh.stockCode\n" +
            "   WHERE odh.stockCode = :stockId AND odh.userId = :userId")
    Mono<StockDealing> getStockInformationByUserWise(@Param("stockId") Long stockId, @Param("userId") Integer userId);

*/

}
