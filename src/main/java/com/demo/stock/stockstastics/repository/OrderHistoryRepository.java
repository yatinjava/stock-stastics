package com.demo.stock.stockstastics.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;

import com.demo.stock.stockstastics.entity.StocksResponse;
import com.demo.stock.stockstastics.services.impl.OrderHistoryMapper;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Flux;

@Repository
@RequiredArgsConstructor
public class OrderHistoryRepository  {

	@Autowired
	DatabaseClient databaseClient;
	
	public Flux<StocksResponse> getOrderHistoryBasedonUser(Integer userId) {
		OrderHistoryMapper mapper = new OrderHistoryMapper();
		/*Flux<StocksResponse> oredrHistory =  databaseClient.sql(
				        "SELECT " 
		                + "sd.stock_name as stockName , odh.quantity as quantity, "
						+ "odh.purchasedDate as purchasedDate, odh.purchasedPrice as purchasedPrice "
						+ "FROM orderhistory odh "
						+ "JOIN stock_details sd  ON sd.stocks_id = odh.stockCode "
						+ "WHERE odh.userId = :userId")
				.bind("userId", userId)
				.map((row, object) -> 
					            new StocksResponse(row.get("stockName", String.class),
								row.get("quantity", Integer.class), row.get("purchasedDate", Instant.class),
								row.get("purchasedPrice", Double.class)
							
				)).all();*/
				
		Flux<StocksResponse> oredrHistory =  databaseClient.sql(
		        "SELECT " 
                + "sd.stock_name as stockName , odh.quantity as quantity, "
				+ "odh.purchasedDate as purchasedDate, odh.purchasedPrice as purchasedPrice "
				+ "FROM orderhistory odh "
				+ "JOIN stock_details sd  ON sd.stocks_id = odh.stockCode "
				+ "WHERE odh.userId = :userId")
		.bind("userId", userId)
		.map(mapper::apply).all().switchIfEmpty(Flux.error(new RuntimeException("order not found!")));
		return oredrHistory;
		

	}

}
