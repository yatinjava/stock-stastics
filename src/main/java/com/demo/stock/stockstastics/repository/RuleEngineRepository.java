package com.demo.stock.stockstastics.repository;

import com.demo.stock.stockstastics.entity.RuleEngine;
import org.springframework.data.repository.reactive.ReactiveSortingRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

import java.util.List;


    @Repository
    public interface RuleEngineRepository extends ReactiveSortingRepository<RuleEngine, Integer> {

        /**
         * @param userId
         * @param runStatus
         * @return RuleEngine list for user id
         */
        Flux<RuleEngine> findRuleEngineByUserIdAndRunStatus(Integer userId, Integer runStatus);

    }

