package com.demo.stock.stockstastics.repository;

import com.demo.stock.stockstastics.exception.GlobalException;
import com.demo.stock.stockstastics.model.InvestorDetails;
import com.demo.stock.stockstastics.model.StockAnalysis;
import com.demo.stock.stockstastics.model.StockDealing;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Instant;

@Repository
@RequiredArgsConstructor
public class CustomStockDetailRepository {

    @Autowired
    DatabaseClient databaseClient;

    public Mono<StockDealing> getStockInformationByUserWise(Long stockId, Integer userId) {
        Mono<StockDealing> stockName = databaseClient.sql(
                "SELECT "
                        + "sd.stock_name as stockName , odh.quantity as purchaseQnty, "
                        + "odh.purchasedDate as purchaseDate, odh.purchasedPrice as purchasedPrice, sd.stock_price as currentPrice,  "
                        + "0.0 as totalDealingPrice,now() as dealingDate,'' as dealingType "
                        + "FROM orderhistory odh "
                        + "JOIN stock_details sd  ON sd.stocks_id = odh.stockCode "
                        + "WHERE odh.stockCode = :stockId AND odh.userId = :userId")
                .bind("stockId", stockId)
                .bind("userId", userId)
                .map((row, rowMetadata) -> new StockDealing(
                        row.get("stockName", String.class),
                        row.get("purchaseQnty", Double.class),
                        row.get("purchaseDate", Instant.class),
                        row.get("purchasedPrice", Double.class),
                        row.get("currentPrice", Double.class),
                        row.get("totalDealingPrice", Double.class),
                        row.get("dealingDate", Instant.class),
                        row.get("dealingType", String.class)
                )).one()
                .switchIfEmpty(Mono.error(new RuntimeException("StockDeal not found!")));
        return stockName;
    }

    public Flux<InvestorDetails> getInvestorDetails() {
        Flux<InvestorDetails> investor = databaseClient
                .sql("SELECT " + "userId, sum(purchasedPrice) as totalInvestments," + " sum(quantity) as totalStocks "
                        + "from orderhistory " + "group by userId")
                .map((row, rowMetadata) -> new InvestorDetails(row.get("userId", Integer.class),
                        row.get("totalInvestments", Double.class), row.get("totalStocks", Double.class)))
                .all().switchIfEmpty(Flux.error(new RuntimeException("Investors not found!")));
        return investor;
    }

    public Flux<StockAnalysis> getStockAnalysisDetails(Long stockId) {
        Flux<StockAnalysis> stockAnalysisFlux = databaseClient
                .sql("SELECT YEAR(sh.update_date) AS year, max(sh.stock_price) AS maxPrice, "
                        + "min(sh.stock_price) AS lowPrice, sh.stockId FROM stockprice_history sh "
                        + "WHERE sh.stockId = :stockId "
                        + "GROUP BY year")
                .bind("stockId", stockId)
                .map((row, rowMetadata) -> new StockAnalysis(row.get("year", Integer.class),
                        row.get("maxPrice", Double.class), row.get("lowPrice", Double.class),row.get("stockId", Long.class)))
                .all().switchIfEmpty(Mono.defer(() -> Mono.error(new GlobalException(HttpStatus.NOT_FOUND, String.format("Stock not found for the provided stockId :: %s", stockId)))));
        return stockAnalysisFlux;
    }


    /*
     * public Mono<Pet> getOne(String name) { return databaseClient
     * .execute().sql("SELECT name, sound FROM pets WHERE name = :name")
     * .bind("name", name) .map((row, rowMetadata) -> new Pet( row.get("name",
     * String.class), row.get("sound", String.class) )).one()
     * .switchIfEmpty(Mono.error(new RuntimeException("Pet not found!"))); }
     */
}
