package com.demo.stock.stockstastics.exception;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ErrorResponse {

    private List<java.lang.Error> errors = new ArrayList<>();
}
