package com.demo.stock.stockstastics.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Error extends java.lang.Error {

    @NonNull private int code;
    @NonNull private String message;
    @NonNull private String description;
}
