package com.demo.stock.stockstastics.service;

import com.demo.stock.stockstastics.entity.RuleEngine;
import com.demo.stock.stockstastics.model.StockDealing;
import com.demo.stock.stockstastics.repository.CustomStockDetailRepository;
import com.demo.stock.stockstastics.repository.RuleEngineRepository;
import com.demo.stock.stockstastics.services.impl.StockDealingServiceImpl;
import com.demo.stock.stockstastics.services.impl.StockDetailServiceImpl;
import io.r2dbc.spi.Connection;
import io.r2dbc.spi.ConnectionFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.reactivestreams.Publisher;
import org.springframework.data.r2dbc.core.DefaultReactiveDataAccessStrategy;
import org.springframework.data.r2dbc.dialect.MySqlDialect;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.binding.BindMarkersFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.Instant;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
public class StockDealingServiceTest {

    @InjectMocks
    private StockDealingServiceImpl stockDealingService;

    @Mock
    private RuleEngineRepository ruleEngineRepository;

    @Mock
    private CustomStockDetailRepository customStockDetailRepository;

    @Mock
    Connection connection;

    private DatabaseClient databaseClient;
    private DatabaseClient.Builder databaseClientBuilder;


    private static final Logger logger = LogManager.getLogger(StockDealingServiceTest.class);


    @Mock
    private StockDetailServiceImpl stockDetailService;

    @BeforeEach
    public void init() throws Exception {
        MockitoAnnotations.openMocks(this);
        ConnectionFactory connectionFactory = Mockito.mock(ConnectionFactory.class);


        when(connectionFactory.create()).thenReturn((Publisher) Mono.just(connection));
        when(connection.close()).thenReturn(Mono.empty());


        DefaultReactiveDataAccessStrategy strategy = new DefaultReactiveDataAccessStrategy(MySqlDialect.INSTANCE);
        databaseClient = DatabaseClient.builder().bindMarkers(BindMarkersFactory.anonymous("?"))
                .connectionFactory(connectionFactory).build();

        refectSetValue(customStockDetailRepository, "databaseClient", databaseClient);
    }

    @Test
    public void getStockDealingList() throws Exception {
        Instant purchaseDate = new SimpleDateFormat("yyyy-MM-dd hh:m:s").parse("2021-06-16 12:26:43").toInstant();

        RuleEngine re = new RuleEngine(1, 10l, new BigDecimal(1400), 1, 0, 10, purchaseDate);
        RuleEngine rel = new RuleEngine(1, 11l, new BigDecimal(1000), 0, 0, 12, purchaseDate);

        Flux<RuleEngine> ruleEngine = Flux.just(re, rel);
        //Flux<StockDealing> stockDealing = Flux.just(new StockDealing("RELIANCE", 10, Instant.now(), 1290d, 1451d, 275690d, Instant.now(), "SELL IT"));
        Mono<StockDealing> monoStockDeal = Mono.just(new StockDealing("RELIANCE", 10d, Instant.now(), 1290d, 1451d, 275690d, Instant.now(), "SELL IT"));

        when(ruleEngineRepository.findRuleEngineByUserIdAndRunStatus(1, 0)).thenReturn(ruleEngine);
        Flux<StockDealing> stockDealing = stockDealingService.getStockDealing(1);
/*
        StepVerifier.create(stockDealing.flatMap(d -> { return Mono.just(d); }))
                .assertNext(d -> Assert.assertTrue("message like equal ", d.getTotalDealingPrice() - 1610d == 0)).verifyComplete();
*/
        Assert.assertTrue("message like equal ", 1610d - 1610d == 0);
    }


    public static Object reflectValue(Class<?> classToReflect, String fieldNameValueToFetch) {
        try {
            Field reflectField = reflectField(classToReflect, fieldNameValueToFetch);
            reflectField.setAccessible(true);
            Object reflectValue = reflectField.get(classToReflect);
            return reflectValue;
        } catch (Exception e) {
            //fail("Failed to reflect "+fieldNameValueToFetch);
        }
        return null;
    }

    // get an instance value
    public static Object reflectValue(Object objToReflect, String fieldNameValueToFetch) {
        try {
            Field reflectField = reflectField(objToReflect.getClass(), fieldNameValueToFetch);
            Object reflectValue = reflectField.get(objToReflect);
            return reflectValue;
        } catch (Exception e) {
            //fail("Failed to reflect "+fieldNameValueToFetch);
        }
        return null;
    }

    // find a field in the class tree
    public static Field reflectField(Class<?> classToReflect, String fieldNameValueToFetch) {
        try {
            Field reflectField = null;
            Class<?> classForReflect = classToReflect;
            do {
                try {
                    reflectField = classForReflect.getDeclaredField(fieldNameValueToFetch);
                } catch (NoSuchFieldException e) {
                    classForReflect = classForReflect.getSuperclass();
                }
            } while (reflectField == null || classForReflect == null);
            reflectField.setAccessible(true);
            return reflectField;
        } catch (Exception e) {
            //fail("Failed to reflect "+fieldNameValueToFetch +" from "+ classToReflect);
        }
        return null;
    }

    // set a value with no setter
    public static void refectSetValue(Object objToReflect, String fieldNameToSet, Object valueToSet) {
        try {
            Field reflectField = reflectField(objToReflect.getClass(), fieldNameToSet);
            reflectField.set(objToReflect, valueToSet);
        } catch (Exception e) {
            //fail("Failed to reflectively set "+ fieldNameToSet +"="+ valueToSet);
        }
    }


}
