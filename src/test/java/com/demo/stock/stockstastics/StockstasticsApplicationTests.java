package com.demo.stock.stockstastics;

import com.demo.stock.stockstastics.controller.StockController;
import com.demo.stock.stockstastics.exception.GlobalErrorAttributes;
import com.demo.stock.stockstastics.model.Investors;
import com.demo.stock.stockstastics.model.StockDealing;
import com.demo.stock.stockstastics.services.InvestorDetailService;
import com.demo.stock.stockstastics.services.StockDealingService;
import com.demo.stock.stockstastics.services.StockDetailService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebFluxTest(StockController.class)
class StockstasticsApplicationTests {

    @Autowired
    private WebTestClient webTestClient;

    @MockBean
    private StockDetailService stockDetailService;

    @MockBean
    private StockDealingService stockDealingService;

    @MockBean
    private InvestorDetailService investorDetailService;

    @MockBean
    private GlobalErrorAttributes globalErrorAttributes;



    @Test
    public void getStockDealingListSucess() throws ParseException {

        Instant purchaseDate = new SimpleDateFormat("dd/MM/yyyy").parse("31/12/2020").toInstant();
        Instant dealingDate = new SimpleDateFormat("dd/MM/yyyy").parse("01/01/2021").toInstant();


        Flux<StockDealing> stockDealing = Flux.just(new StockDealing("TCS", 12d, purchaseDate, 1250d, 1300d, 27567d, dealingDate, "SELL IT"),
                new StockDealing("RELIANCE", 15d, purchaseDate, 1290d, 1400d, 275690d, dealingDate, "BUY IT"));
        when(stockDealingService.getStockDealing(1)).thenReturn(stockDealing);
        Flux<StockDealing> responseBody = webTestClient.get().uri("/stockDealing/1").exchange().expectStatus().isOk().returnResult(StockDealing.class).getResponseBody();

        StepVerifier.create(responseBody)
                .expectSubscription()
                .expectNext(new StockDealing("TCS", 12d, purchaseDate, 1250d, 1300d, 27567d, dealingDate, "SELL IT"))
                .expectNext(new StockDealing("RELIANCE", 15d, purchaseDate, 1290d, 1400d, 275690d, dealingDate, "BUY IT"))
                .verifyComplete();

    }

    @Test
    public void getStockDealingListWithWrongResource() {
        Flux<StockDealing> stockDealing = Flux.just(new StockDealing("TCS", 12d, Instant.now(), 1250d, 1300d, 27567d, Instant.now(), "SELL IT"));
        when(stockDealingService.getStockDealing(1)).thenReturn(stockDealing);
        webTestClient.get().uri("/stockDealing123/1").exchange().expectStatus().isBadRequest();
    }

    @Test
    public void getTradersOverview() throws ParseException {

        Mono<Investors> investorsMono = Mono.just(new Investors());
        when(investorDetailService.getInvestorDetails()).thenReturn(investorsMono);

        Flux<Investors> responseBody = webTestClient.get().uri("/tradersOverview").exchange().expectStatus().isOk().returnResult(Investors.class).getResponseBody();
        assertNotNull(responseBody);

    }

}
