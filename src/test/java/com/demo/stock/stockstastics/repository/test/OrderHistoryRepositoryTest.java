package com.demo.stock.stockstastics.repository.test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.Instant;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.r2dbc.core.DatabaseClient;
import org.springframework.r2dbc.core.DatabaseClient.GenericExecuteSpec;
import org.springframework.r2dbc.core.RowsFetchSpec;

import com.demo.stock.stockstastics.entity.StocksResponse;
import com.demo.stock.stockstastics.repository.OrderHistoryRepository;
import com.demo.stock.stockstastics.services.impl.OrderHistoryMapper;

import reactor.core.publisher.Flux;

@SpringBootTest
class OrderHistoryRepositoryTest {

	@Mock
	DatabaseClient databaseClient;
	
	@InjectMocks
	OrderHistoryRepository repository;
	
	
	
	
	
	@Test
	public void testGetOrderHistoryBasedonUser() {
		GenericExecuteSpec executeSpec = Mockito.mock(GenericExecuteSpec.class);
		//GenericExecuteSpec spec = Mockito.mock(GenericExecuteSpec.class);
		RowsFetchSpec<StocksResponse> value = Mockito.mock(RowsFetchSpec.class);
		StocksResponse stockResponse = new StocksResponse();
		stockResponse.setQuantity(100d);
		stockResponse.setPurchasedPrice(1000d);
		stockResponse.setStockName("RIL");
		Flux<StocksResponse> response = Flux.just(stockResponse);
		
		//Mockito.when(value.all()).thenReturn(response );
		//Mockito.when(mapper.apply(Mockito.any(), Mockito.anyObject())).thenReturn(stockResponse);
		//Mockito.when(executeSpec.map(mapper::apply)).thenReturn(value );
		
		Mockito.when(executeSpec.bind(Mockito.anyString(), Mockito.anyObject())).thenReturn(executeSpec );
		Mockito.when(databaseClient.sql(Mockito.anyString())).thenReturn(executeSpec );
		
		Flux<StocksResponse> orderHistoryResponse = repository.getOrderHistoryBasedonUser(1);
	
	
	
	
	}

}
